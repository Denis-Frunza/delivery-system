FROM python:3.10-slim as builder

WORKDIR /code

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update \
  && apt-get -y install gcc postgresql \
  && apt-get clean

COPY requirements/ /code/requirements
COPY requirements.txt /code/requirements.txt
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /code/wheels -r /code/requirements.txt

FROM python:3.10-slim

WORKDIR /code

COPY --from=builder /code/wheels /wheels
COPY --from=builder /code/requirements.txt .

RUN pip install --no-cache /wheels/*
COPY . .

CMD ["python", "delivery_system/manage.py", "runserver", "0.0.0.0:8080"]